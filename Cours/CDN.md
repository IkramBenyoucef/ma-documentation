# Cinquième cours: CDN
## Présentation

CDN: Content Delivry Network: reseau mis à dispositionde contenu de ressource.

Certaines parties d'un site sont statiques et d'autres sont dynamiques:

- Statique: HTML, CSS, JS (-->parce que fichier codé une fois en dure). Ce sont que des fichiers texte, image ou video.
- Dynamique: base de données, JS ou back office. ils vot gérer des datas qui vont etre envoyés dans les parties statiques.


## Fonctionnement
Pour rendre accesible un site à un maximum de personnes, on va, à travers le monde envoyer des copies de fichier statique du site.
> CDN (content delivry network) copier la meme ressource pour le mettre à disposition à travers le monde.
## Comment deployer un site sur CDN

En 2018 le meilleur CDN au monde etait celui de Google = Firebase.

### Debuter
Avant de commencer il faut s'inscrire et rentrer certaines informations:
1. Ajouter un projet
2. Pas besoin activer
3. Hosting= CDN= envoyé dans le monde entier de maniere securisé SSL

### Premiere étape
1. Copier et coller dans la console de Webstorm le fichier firebase
2. Copier et coller firebase login et init hosting
3. Choisir un projet firebase

### Seconde étape
1. Créer un dossier Pulic et déplacer tout les dossiers dedans
2. Couper et coller le fichier Home/ Index à la racine du dossier

Pour finir, on rajoute Firebase deploy à la console pour envoyer le site sur internet, un lien URL vers la page internet est donné il faudra le reupérer.

### Troisième étape
Des dossiers et des fichiers sont rajoutés automatiquement apres avoir déployer son site sur un CDN.

- Les fichiers doivent etre Git add
- Les dossiers doivent etre .Gitignore



(cf video cdn)