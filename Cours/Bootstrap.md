# Deuxieme cours: Definition Bootstrap et création d'un site web
## Premiere partie: Definition Bootstrap
Bootstrap est une collection d'outils utiles à la création du design de sites et d'applications web. C'est un ensemble qui contient des codes HTML et CSS, des formulaires, boutons, outils de navigation et autres éléments interactifs, ainsi que des extensions JavaScript en option. 

## Deuxieme partie: Création d'un site internet
Un **navigateur** est un logiciel qui va nous permettre de surfer sur le web.

Avant de créer un site web, bien se poser la question: "combien de pages je souaite avoir et commment vais-je structurer ma page?"

3 types de pages: 
- Le **landing**
- **About**
- **FAQ**

Pour crée une page --> **directory**

Pour **inserer un logo** --> aller dans **Asset** --> **img**

Nouvelles balises: 
- **Section** (le coeur des informations).
Il peut y avoir un **header** et un **footer**. Dans chaque section, le h2 doit se trouver dans le header. Le footer sert à ajouter des informations supplémentaires. Ils permettent a Google de comprendre qu'il y a plusieurs parties.

- Les **Badges** servent à donner des informations en plus. Par exemple, à surligner un mot important ou à ajouter un compteur à un lien ou à un bouton.

- Le **Breadcrumb** est un élément qui indique à l'utilisateur où il se situe dans la hiérarchie du site, par rapport à la page d'accueil.
- Les **Buttoms** s'occupent des boutons sur lesquels on clique.

- Le **Carousel** nous permet de mettre des photos les une apres les autres.

- Le **collapse** est une fiche de details. Il peut par exemple cacher du contenu.

- Le **Containers** est une Div principale où il faut mettre tous le contenu. Il réajuste notre texte. 

Attention: il ne peut y avoir qu'un seul Containers par page (comme pour le h1).

Exmple:
``` 
<section>
    <header>
        <h2>title</h2>
    </header>
    <footer>
    </footer>
</section>