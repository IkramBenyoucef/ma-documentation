#9eme cours: Les fonctions

## Définition

Une fonction c'est quelque chose qui déclencher des ordres (les ordres declenché vont etre dans la zone des []).

Le mot clé pour declarer une fonction --> **Function**.

On peut aussi utiliser **Let** ou **Const**.

Quand on déclare une fonction on ne met pas de  **=**, on met directement des **()**  qui vont servir à mettre des parametres. Ensuite nous mettons des **[]** dans lequel la fonction va s'executer.

## Void
 Une fonction qui ne retourne rien est une fonction **Void** (=vide, néant).
 
 > Exemple:
>
> Function coucou () [
>
>console.log ('coucou')
>]
>
>**return void**

## Rest operator

>Function randomAction (...z) {
>
>console.log (z)
>
>z.map (value => console.log (value)) 
>
>}

Les 3 points (...) vont prendre tous les arguments qu'il y a dans le **z** et cela va les mettre dans un tableau.



