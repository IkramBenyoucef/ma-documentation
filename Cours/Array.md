# 7eme cours: Array

## Array
Les array sont des tableaux. C'est un objet, un container dans lequel on va mettre d'autre chose. Cela sert à stocker.
Débuter une page **console.log (' ')**

Lier le JS avec le HTML en mettant une balise **< script >** sur la page HTML.

Pour écrire un Array: **const tab = [ ];**

Les tabl sont des suites d'objet (ex: firstName: ikram, age: 20)

Une table se crée par une **const**.
Pour déclarer une table--> pour stocker des nombres, des srting, des boolean (ttrue,false), prédicat (ex: 4 < 5)

Les indexes permettent de récupérer des informations.

## Opérateurs

Toujours à la fin des tab.

  - **tab.forEach** () Une boucle qui va faire une opération. Il exécute ce qu'il y a dans la fonction anonymr.

>(**element =>** (function qui va déclancher du code) **console.log (element)** )

- **tab.map**: il va modifier la facon dont fonctionne l'objet, il permet de parcourir un tableau.

>Exemple: 
>**const tableauDesAges = tab.map (element => element.age)**
>
>**console.log (tableauDesAges)**

- **Filter**: permet de filtrer
>Exemple: **const moinsDe40 = tab.filter (element => element.age < 40)**
>
>**console.log (moinsDe40);**

- **Sort**: permet de réorganiser un tableau
> Exemple: **const orderedTab = tab.sort (compareFn (a,b) => a.age - b.age)**
>
>**console.log (orderedTab);**

- **Every**: tout les elements d'un tableau répondent à une condition.
> Exemple: **const areAllMajors = tab.every (element => element.age > 18)**
>
>**console.log (areAllMajor)**

- **Some**: un seul element du tableau répond à la demande (l'inverse d'every)

- **Push**: il permet de rajouter des elements dans le tableau

> Exemple:**const newPerson = { firstName: ben, age:32}**
>
>**classe.push (newPerson);**
>
>**console.log (classe)**

#### Classer en fonction de l'ordre alphabétique
- Exemple: **const tabByName = classe.sort (compareFn (a,b) => { if (a.firstName < b.firstName**

    **{return -1}**
    
    **if (a.firstName > b.firstName )**
    
    **{return 1;} } );**
    
    

