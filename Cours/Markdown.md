# Premier cours: Markdown

## Qu'est ce que le Markdown?
Le Markdown est un language de balisage léger. Son but est d'offrir une syntaxe facile à lire et à écrire. 

Pour structurer une page en Markdown il faut un **#1** (h1 en HTML) principale, puis diviser ma page de sous parties.
## Backticks
Exemles de Backticks: 

```Markdown| HTML| Rendu
 
 |#|    | h1   | Titre principale
 
 |##|   | h2   | Sous titres
 
 |** . **| <strong> | Texte gras
 
 | _ |  |<|>| Texte italique
```

### Creer un dossier Markdown
1. Aller dans File
2. Puis dans new --> File

## Qu'est ce que le Gitlab?
- Le Gitlab ainsi que le Github, sont des sites internet qui utilisent le protocle Git.
Cela va leur permettre de stocker du contenu informatique ou autre.
- Le Git est protocole qui permet de stocker des informations.
### Créer un nouveau projet
1. Aller sur Gitlab et cliquer sur **+** (new)
2. Séléctionner **New project**
3. Entrer le nom du projet dans **Project Name**
4. Enfin cocher la case **Public** et **Initialize repository with a README**

### Stocker un document markdown de Webstorm à Gitlab
> Sur Gitlab
1. Dans **Ma Documentation** séléctionner **Clone**
2. Puis copier le lien **Clone with HTTPS**

> Sur Webstorm
1. Aller sur la page d'acceuil de Webstorm
2. Séléctionner **Check Out**
3. Coller l'URL 


>Commit
1. Aller sur **VCS**
2. cliquer sur **Commit** (ou Ctrl+K)
3. Mettre une description dans **commit Message**
4. Decocher **Perform code analysis** et **Check TODO**
>Push
1. Aller dans **VCS**
2. Puis cliquer sur **GIT**
3. Enfin cliqur sur **Push** (ou Ctrl+ Maj+ K)


## Vocabulaires
- SEO: Search Engine Optimization (optimisation pour les moteurs de recherche)
- WYSIWYG: "What I see, What I get" désigne en informatique une interface utilisateur qui permet de composer visuellement le résultat voulu, typiquement pour un logiciel de mise en page, un traitement de texte ou d’image
- Bootstrap: Framework 
